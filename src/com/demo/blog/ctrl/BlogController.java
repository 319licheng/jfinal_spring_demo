package com.demo.blog.ctrl;

import com.demo.blog.interceptor.BlogInterceptor;
import com.demo.blog.model.Blog;
import com.demo.blog.service.BlogService;
import com.demo.blog.validator.BlogValidator;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.spring.Inject;

/**
 * BlogController
 * 所有 sql 与业务逻辑写在 Model 或 Service 中，不要写在 Controller 中，养成好习惯，有利于大型项目的开发与维护
 */
@Before(BlogInterceptor.class)
public class BlogController extends Controller {
	
	@Inject.BY_TYPE
	private BlogService service;
	
	public void index() {
		setAttr("blogPage", service.paginate(getParaToInt(0, 1), 10));
		render("blog.html");
	}
	
	public void add() {
	}
	
	@Before({BlogValidator.class})
	public void save() {
		service.save(getModel(Blog.class));
		redirect("/blog");
	}
	
	public void edit() {
		setAttr("blog", service.findById(getParaToInt()));
	}
	
	@Before(BlogValidator.class)
	public void update() {
		service.update(getModel(Blog.class));
		redirect("/blog");
	}
	
	public void delete() {
		service.deleteById(getParaToInt());
		redirect("/blog");
	}
}


